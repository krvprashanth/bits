Title: Nueva sección: Debian sobre el terreno («Debian in the Field»)
Slug: debian-in-the-field
Date: 2015-11-15 09:00
Author: Laura Arjona Reina, Bernelle Verster
Translator: Adrià García-Alzórriz
Tags: DiF, Debian in the Field, DebConf16
Lang: es
Status: draft

«Debian sobre el terreno» («Debian in the Field» en inglés) muestra algunas aplicaciones de Debian
(y más ampliamente, de software libre) para una amplia audiencia (desde profesionales de TI o miembros de Debian
a personas u organizaciones que podrían considerar a los ordenadores como «aquellas cosas que hacen
cosas útiles»).

La web de Debian ya tiene una sección [«¿Quién usa 
Debian?»](https://www.debian.org/users); aquí en el blog pretendemos ofrecer
varias y más detalladas experiencias de algunos de esos usuarios.

Estos usos sobre el terreno se comparten para mostrar Debian en uso y no 
expresan ni la visión ni la aprobación por parte del Proyecto Debian.

Si quieres colaborar con artículos en esta sección «Debian sobre el terreno»
visita la
[página wiki de bits.debian.org](https://wiki.debian.org/Teams/Publicity/bits.debian.org).
