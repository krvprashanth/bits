Title: 新的 Debian 开发者和维护者 (2022年7月 至 8月)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

  * Sakirnth Nagarasa (sakirnth)
  * Philip Rinn (rinni)
  * Arnaud Rebillout (arnaudr)
  * Marcos Talau (talau)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Xiao Sheng Wen
  * Andrea Pappacoda
  * Robin Jarry
  * Ben Westover
  * Michel Alexandre Salim

祝贺他们！

