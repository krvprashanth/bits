Title: Nya Debianutvecklare och Debian Maintainers (November och December 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Douglas Andrew Torrance (dtorrance)
  * Mark Lee Garrett (lee)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Lukas Matthias Märdian
  * Paulo Roberto Alves de Oliveira
  * Sergio Almeida Cipriano Junior
  * Julien Lamy
  * Kristian Nielsen
  * Jeremy Paul Arnold Sowden
  * Jussi Tapio Pakkanen
  * Marius Gripsgard
  * Martin Budaj
  * Peymaneh
  * Tommi Petteri Höynälänmaa

Grattis!

