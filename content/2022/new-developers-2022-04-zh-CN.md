Title: 新的 Debian 开发者和维护者 (2022年3月 至 4月)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

  * Henry-Nicolas Tourneur (hntourne)
  * Nick Black (dank)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Jan Mojžíš
  * Philip Wyett
  * Thomas Ward
  * Fabio Fantoni
  * Mohammed Bilal
  * Guilherme de Paula Xavier Segundo

祝贺他们！

