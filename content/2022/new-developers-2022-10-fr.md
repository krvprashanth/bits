Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Abraham Raji (abraham)
  * Phil Morrell (emorrp1)
  * Anupa Ann Joseph (anupa)
  * Mathias Gibbens (gibmat)
  * Arun Kumar Pariyar (arun)
  * Tino Didriksen (tinodidriksen)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Gavin Lai
  * Martin Dosch
  * Taavi Väänänen
  * Daichi Fukui
  * Daniel Gröber
  * Vivek K J
  * William Wilson
  * Ruben Pollan

Félicitations !

