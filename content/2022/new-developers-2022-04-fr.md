Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Henry-Nicolas Tourneur (hntourne)
  * Nick Black (dank)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Jan Mojžíš
  * Philip Wyett
  * Thomas Ward
  * Fabio Fantoni
  * Mohammed Bilal
  * Guilherme de Paula Xavier Segundo

Félicitations !

