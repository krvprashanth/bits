Title: New Debian Developers and Maintainers (May and June 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Geoffroy Berret (kaliko)
  * Arnaud Ferraris (aferraris)

The following contributors were added as Debian Maintainers in the last two months:

  * Alec Leanas
  * Christopher Michael Obbard
  * Lance Lin
  * Stefan Kropp
  * Matteo Bini
  * Tino Didriksen

Congratulations!

