Title: ITP Prizren Platinum Sponsor of DebConf22
Slug: debconf22-itp-prizren-platinum
Date: 2022-04-19 09:00
Author: The Debian Publicity Team
Artist: ITP Prizren
Tags: debconf22, debconf, sponsors, itp
Image: /images/debconf22-banner-700px.png
Status: published

[![itplogo](|static|/images/itp.png)](https://itp-prizren.com)

We are very pleased to announce that
[**ITP - Innovation and Training Park Prizren**](https://itp-prizren.com)
has committed to supporting [DebConf22](https://debconf22.debconf.org) as a
**Platinum sponsor**. Also, ITP Prizren will host the Conference for all 15
days!

The [ITP Prizren](https://itp-prizren.com) intends to be a changing and
boosting element in the area of ICT, agro-food and creatives industries, through
the creation and management of a favourable environment and efficient services
for SMEs, exploiting different kinds of innovations that can contribute to
Kosovo to improve its level of development in industry and research, bringing
benefits to the economy and society of the country as a whole.

ITP Prizren is a focal point in the Balkan region for innovation, business and
skills development, and a source of innovative and successful ideas.

With this commitment as Platinum Sponsor, ITP Prizren is contributing to make
possible our annual conference, and directly supporting the progress of Debian
and Free Software, helping to strengthen the community that continues to
collaborate on Debian projects throughout the rest of the year.

Thank you very much ITP Prizren, for your support of DebConf22!

## Become a sponsor too!

[DebConf22](https://debconf22.debconf.org) **will take place from July 17th to
24th, 2022 at the [Innovation and Training Park (ITP)](https://itp-prizren.com)
in Prizren, Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.

And DebConf22 is still accepting sponsors!
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf22 website at
[https://debconf22.debconf.org/sponsors/become-a-sponsor](https://debconf22.debconf.org/sponsors/become-a-sponsor).

![DebConf22 banner open registration](|static|/images/debconf22-banner-700px.png)
