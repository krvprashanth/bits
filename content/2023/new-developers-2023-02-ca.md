Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Sahil Dhiman (sahil)
  * Jakub Ružička (jru)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Josenilson Ferreira da Silva
  * Ileana Dumitrescu
  * Douglas Kosovic
  * Israel Galadima
  * Timothy Pearson
  * Blake Lee
  * Vasyl Gello
  * Joachim Zobel
  * Amin Bandali

Enhorabona a tots!

