Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * François Mazen (mzf)
  * Andrew Ruthven (puck)
  * Christopher Obbard (obbardc)
  * Salvo Tomaselli (ltworf)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Bo YU
  * Athos Coimbra Ribeiro
  * Marc Leeman
  * Filip Strömbäck

¡Felicidades a todos!

