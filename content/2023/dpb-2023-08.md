Title:  Debian Project Bits Volume 1, Issue 1
Slug: debian-project-bits1
Date: 2023-08-05 12:30
Author: Jean-Pierre Giraud,  Joost van Baal-Ilić, Carlos Henrique Lima Melara, Donald Norwood, Paulo Henrique de Lima Santana
Tags: news, debconf23, dpb
Status: published
- - -
**Debian Project Bits**
*Volume 1, Issue 1*
*August 05, 2023*


### Welcome to the inaugural issue of Debian Project Bits!

Those remembering the Debian Weekly News (DwN) will recognize some of the sections here which served as our inspiration.

Debian Project Bits posts will allow for a faster turnaround of some project
news on a monthly basis. The [Debian Micronews](https://micronews.debian.org/)
service will continue to share shorter news items, the [Debian Project News](https://www.debian.org/News/weekly/)
remains as our official newsletter which may move to a biannual archive format.


News
----

#### Debian Day

The Debian Project was [officially
founded](https://wiki.debian.org/DebianHistory) by Ian Murdock on August 16,
1993. Since then we have celebrated our Anniversary of that date each year with
events around the world. We would love it if you could join our revels
this very special year as we have the honor of turning ***_30_***!

Attend or organize a local **[Debian Day](https://wiki.debian.org/DebianDay)**
celebration. You're invited to plan your own event: from Bug Squashing parties
to Key Signing parties, Meet-Ups, or any type of social event whether large or
small.  And be sure to check our [Debian reimbursement How
To](https://wiki.debian.org/Teams/DPL/Reimbursement) if you need such
resources.

You can share your days, events, thoughts, or notes with us and the
rest of the community with the **#debianday** tag that will be used across most
[social media platforms](https://wiki.debian.org/Teams/DebianSocial). See you then!

Events: Upcoming and Reports
----------------------------

### Upcoming

#### Debian 30 anos

The [Debian Brasil Community](https://debianbrasil.org.br/) is organizing the
event [Debian 30 anos](https://debianbrasil.gitlab.io/debian30anos/) to
celebrate the 30th anniversary of the Debian Project.

From August 14 to 18, between 7pm and 22pm (UTC-3) contributors will talk
online in Portuguese and we will live stream on
[Debian Brasil YouTube channel](https://www.youtube.com/DebianBrasilOficial).

#### DebConf23: Debian Developers Camp and Conference

The 2023 Debian Developers Camp \(DebCamp) and Conference
(**[DebConf23](https://debconf23.debconf.org/)**) will be hosted this year in
Infopark, [Kochi, India](https://debconf23.debconf.org/about/kochi/).

DebCamp is slated to run from September 3 through 9, immediately followed by
the larger DebConf, September 10 through 17.

If you are planning on attending the conference this year, now is the time to
ensure your travel documentation, [visa
information](https://lists.debian.org/debconf-announce/2023/07/msg00001.html),
bursary submissions, papers and relevant equipment are prepared. For more
information contact: <debconf@debconf>.

#### MiniDebConf Cambridge 2023

There will be a
[MiniDebConf](https://lists.debian.org/debian-devel-announce/2023/07/msg00002.html)
held in Cambridge, UK, hosted by ARM for 4 days in November: 2 days for a
mini-DebCamp (Thu 23 - Fri 24), with space for dedicated development / sprint /
team meetings, then two days for a more regular MiniDebConf (Sat 25 - Sun 26)
with space for more general talks, up to 80 people.

### Reports

During the last months, the Debian Community has organized some [Bug Squashing Parties](https://wiki.debian.org/BSP):
>
> [Tilburg](https://wiki.debian.org/BSP/2022/11/nl/Tilburg), Netherlands. October 2022.
>
> [St-Cergue](https://wiki.debian.org/BSP/2023/01/ch/St-Cergue), Switzerland. January 2023
>
> [Montreal](https://wiki.debian.org/BSP/2023/02/ca/Montreal), Canada. February 2023
>
> In January, Debian India hosted the [MiniDebConf Tamil Nadu](https://wiki.debian.org/DebianIndia/MiniDebConfTamilNadu2023) in Viluppuram, Tamil Nadu, India (Sat 28 - Sun 26).
>
> The following month, the [MiniDebConf Portugal 2023](https://wiki.debian.org/DebianEvents/pt/2023/MiniDebConfLisbon) was held in Lisbon (12 - 16 February 2023).
>
These events, seen as a _stunning success_ by some of their attendees, demonstrate the vitality of
our community.

#### Debian Brasil Community at Campus Party Brazil 2023

Another edition of [Campus Party Brazil](https://brasil.campus-party.org/cpbr15/)
took place in the city of São Paulo between July 25th and 30th. And one more
time the Debian Brazil Community was present. During the days in the available
space, we carry out some activities such as:

  - Gifts for attendees (stickers, cups, lanyards);
  - Workshop on how to contribute to the translation team;
  - Workshop on packaging;
  - Key signing party;
  - Information about the project;

For more info and a few photos, check out the [organizers'
report](https://debianbrasil.org.br/blog/debian-brasil-campusparty-sp-2023-report/).

#### MiniDebConf Brasília 2023

From May 25 to 27, Brasília hosted the [MiniDebConf Brasília
2023](https://brasilia.mini.debconf.org). This gathering was composed of
various activities such as talks, workshops, sprints, BSPs (Bug Squashing
Party), key signings, social events, and hacking, aimed to bring the community
together and celebrate the world's largest Free Software project: Debian.

For more information please see the
[full report](https://debianbrasil.org.br/blog/minidebconf-brasilia-2023-a-brief-report/)
written by the organizers.

#### Debian Reunion Hamburg 2023

This year the annual [Debian Reunion Hamburg](https://wiki.debian.org/DebianEvents/de/2023/DebianReunionHamburg)
was held from Tuesday 23 to 30 May starting with four days of
hacking followed by two days of talks, and then two more days of hacking. As
usual, people - more than forty-five attendees from Germany, Czechia, France,
Slovakia, and Switzerland - were happy to meet in person, to hack and chat
together, and much more. If you missed the live streams, the
[video recordings](https://meetings-archive.debian.net/pub/debian-meetings/2023/Debian-Reunion-Hamburg/)
are available.

#### Translation workshops from the pt_BR team

The Brazilian translation team, debian-l10n-portuguese, had their first workshop
of 2023 in February with great results. The workshop was aimed at beginners,
working in [DDTP/DDTSS](https://ddtp.debian.org/ddtss).

For more information please see the [full
report](https://debianbrasil.org.br/blog/first-2023-translation-workshop-from-the-pt-BR-team/)
written by the organizers.

And on June 13 another workshop took place to translate
[The Debian Administrator's Handbook)](https://debian-handbook.info). The main
goal was to show beginners how to collaborate in the translation of this
important material, which has existed since 2004. The manual's translations
are hosted on
[Weblate](https://hosted.weblate.org/projects/debian-handbook/#languages).


Releases
--------

#### Stable Release

Debian 12 [bookworm](https://wiki.debian.org/DebianBookworm) was released on
[June 10, 2023](https://www.debian.org/News/2023/20230610). This new version
becomes the stable release of Debian and moves the prior Debian 11
[bullseye](https://wiki.debian.org/DebianBullseye) release to
[oldstable](https://wiki.debian.org/DebianOldStable) status. The Debian
community celebrated the release with 23
[Release Parties](https://wiki.debian.org/ReleasePartyBookworm) all around the
world.

Bookworm's first point release [12.1](https://wiki.debian.org/DebianReleases)
address miscellaneous bug fixes affecting 88 packages, documentation, and
installer updates was made available on [July 22,
2023](https://www.debian.org/News/2023/20230722).

##### RISC-V support
[riscv64](https://wiki.debian.org/RISC-V) has recently been added to the
official Debian architectures for support of 64-bit little-endian
[RISC-V](https://riscv.org) hardware running the Linux kernel. We expect
to have full riscv64 support in Debian 13 trixie. Updates on bootstrap,
build daemon, porterbox, and development progress were recently shared by the
team in a [Bits from the Debian riscv64 porters](https://lists.debian.org/debian-devel-announce/2023/07/msg00003.html)
post.

##### non-free-firmware
The Debian 12 bookworm archive now includes non-free-firmware; please be
sure to update your apt sources.list if your systems requires such components
for operation. If your previous sources.list included non-free for this
purpose it may safely be removed.

##### apt sources.list
The Debian archive holds several components:

- [main](http://www.debian.org/doc/debian-policy/ch-archive#s-main): Contains
  [DFSG](https://www.debian.org/social_contract#guidelines)-compliant packages,
  which do not rely on software outside this area to operate.
- [contrib](http://www.debian.org/doc/debian-policy/ch-archive#s-contrib):
  Contains packages that contain DFSG-compliant software, but have dependencies
  not in main.
- [non-free](http://www.debian.org/doc/debian-policy/ch-archive#s-non-free):
  Contains software that does not comply with the DFSG.
- non-free-firmware: Firmware that is otherwise not part of the Debian system
  to enable use of Debian with hardware that requires such firmware.

###### Example of the sources.list file

    deb http://deb.debian.org/debian bookworm main
    deb-src http://deb.debian.org/debian bookworm main
    
    deb http://deb.debian.org/debian-security/ bookworm-security main
    deb-src http://deb.debian.org/debian-security/ bookworm-security main
    
    deb http://deb.debian.org/debian bookworm-updates main
    deb-src http://deb.debian.org/debian bookworm-updates main

###### Example using the components:

    deb http://deb.debian.org/debian bookworm main non-free-firmware
    deb-src http://deb.debian.org/debian bookworm main non-free-firmware
    
    deb http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware
    deb-src http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware
    
    deb http://deb.debian.org/debian bookworm-updates main non-free-firmware
    deb-src http://deb.debian.org/debian bookworm-updates main non-free-firmware

For more information and guidelines on proper configuration of the apt
source.list file please see the [Configuring Apt Sources -
Wiki](https://wiki.debian.org/SourcesList) page.


Inside Debian
-------------

#### New Debian Members


Please welcome the following newest Debian Project Members:


- Marius Gripsgard \(mariogrip)
- Mohammed Bilal \(rmb)
- Emmanuel Arias \(amanu)
- Robin Gustafsson \(rgson)
- Lukas Märdian \(slyon)
- David da Silva Polverari \(polverari)

To find out more about our newest members or any Debian Developer, look
for them on the [Debian People list](https://nm.debian.org/public/people/).


Security
--------


Debian's Security Team releases current advisories on a daily basis.
Some recently released advisories concern these packages:

[trafficserver](https://www.debian.org/security/2023/dsa-5435-2)
Several vulnerabilities were discovered in Apache Traffic Server, a
reverse and forward proxy server, which could result in information
disclosure or denial of service.

[asterisk](https://www.debian.org/security/2023/dsa-5438)
A flaw was found in Asterisk, an Open Source Private Branch Exchange. A
buffer overflow vulnerability affects users that use PJSIP DNS resolver.
This vulnerability is related to CVE-2022-24793. The difference is that
this issue is in parsing the query record `parse_query()`, while the issue
in CVE-2022-24793 is in `parse_rr()`. A workaround is to disable DNS
resolution in PJSIP config (by setting `nameserver_count` to zero) or use
an external resolver implementation instead.

[flask](https://www.debian.org/security/2023/dsa-5442)
It was discovered that in some conditions the Flask web framework may
disclose a session cookie.

[chromium](https://www.debian.org/security/2023/dsa-5456)
Multiple security issues were discovered in Chromium, which could result
in the execution of arbitrary code, denial of service or information
disclosure.

Other
-----


#### Popular packages

[gpgv](https://packages.debian.org/stable/gpgv) - GNU privacy guard
signature verification tool. _99,053_ installations.
&nbsp;&nbsp;&nbsp;&nbsp;gpgv is actually a stripped-down version of gpg which
is only able to check signatures. It is somewhat smaller than the fully-blown
gpg and uses a different (and simpler) way to check that the public keys used
to make the signature are valid. There are no configuration files and only a
few options are implemented.

[dmsetup](https://packages.debian.org/stable/dmsetup) - Linux Kernel Device
Mapper userspace library. _77,769_ installations.
&nbsp;&nbsp;&nbsp;&nbsp;The Linux Kernel Device Mapper is the LVM (Linux
Logical Volume Management) Team's implementation of a minimalistic kernel-space
driver that handles volume management, while keeping knowledge of the
underlying device layout in user-space. This makes it useful for not only LVM,
but software raid, and other drivers that create "virtual" block devices.

[sensible-utils](https://packages.debian.org/stable/sensible-utils) - Utilities
for sensible alternative selection. 96,001 daily users.
&nbsp;&nbsp;&nbsp;&nbsp;This package provides a number of small utilities which
are used by programs to sensibly select and spawn an appropriate browser,
editor, or pager. The specific utilities included are: sensible-browser
sensible-editor sensible-pager.

[popularity-contest](https://packages.debian.org/stable/popularity-contest) -
The popularity-contest package. 90,758 daily users.
&nbsp;&nbsp;&nbsp;&nbsp;The popularity-contest package sets up a cron job that
will periodically anonymously submit to the Debian developers statistics about
the most used Debian packages on the system. This information helps Debian
make decisions such as which packages should go on the first CD. It also lets
Debian improve future versions of the distribution so that the most popular
packages are the ones which are installed automatically for new users.


#### New and noteworthy packages in unstable

[Toolkit for scalable simulation of distributed applications](https://packages.debian.org/unstable/main/libsimgrid3.34)
&nbsp;&nbsp;&nbsp;&nbsp; SimGrid is a toolkit that provides core
functionalities for the simulation of distributed applications in heterogeneous
distributed environments. SimGrid can be used as a Grid simulator, a P2P
simulator, a Cloud simulator, a MPI simulator, or a mix of all of them. The
typical use-cases of SimGrid include heuristic evaluation, application
prototyping, and real application development and tuning. This package
contains the dynamic libraries and runtime.

[LDraw mklist program](https://packages.debian.org/unstable/main/ldraw-mklist)
&nbsp;&nbsp;&nbsp;&nbsp;3D CAD programs and rendering programs using the LDraw
parts library of LEGO parts rely on a file called parts.lst containing a list
of all available parts. The program ldraw-mklist is used to generate this list
from a directory of LDraw parts.

[Open Lighting Architecture - RDM Responder Tests](https://packages.debian.org/unstable/main/ola-rdm-tests)
&nbsp;&nbsp;&nbsp;&nbsp; The DMX512 standard for Digital MultipleX is used for
digital communication networks commonly used to control stage lighting and
effects. The Remote Device Management protocol is an extension to DMX512,
allowing bi-directional communication between RDM-compliant devices without
disturbing other devices on the same connection. The Open Lighting
Architecture (OLA) provides a plugin framework for distributing DMX512 control
signals. The ola-rdm-tests package provides an automated way to check protocol
compliance in RDM devices.

[parsec-service](https://packages.debian.org/unstable/main/parsec-service)
&nbsp;&nbsp;&nbsp;&nbsp; Parsec is an abstraction layer that can be used to
interact with hardware-backed security facilities such as the Hardware Security
Module (HSM), the Trusted Platform Module (TPM), as well as firmware-backed and
isolated software services. The core component of Parsec is the security
service, provided by this package. The service is a background process that
runs on the host platform and provides connectivity with the secure facilities
of that host, exposing a platform-neutral API that can be consumed into
different programming languages using a client library. For a client library
implemented in Rust see the package librust-parsec-interface-dev.

[Simple network calculator and lookup tool](https://packages.debian.org/unstable/main/ripcalc)
&nbsp;&nbsp;&nbsp;&nbsp;Process and lookup network addresses from the command
line or CSV with ripalc. Output has a variety of customisable formats.

[High performance, open source CPU/GPU miner and RandomX benchmark](https://packages.debian.org/unstable/main/xmrig)
&nbsp;&nbsp;&nbsp;&nbsp;XMRig is a high performance, open source, cross
platform RandomX, KawPow, CryptoNight, and GhostRider unified CPU/GPU miner and
RandomX benchmark.

[Ping, but with a graph - Rust source code](https://packages.debian.org/unstable/main/librust-gping-dev)
&nbsp;&nbsp;&nbsp;&nbsp;This package contains the source for the Rust gping
crate, packaged by debcargo for use with cargo and dh-cargo.



#### Once upon a time in Debian:

2014-07-31 The Technical committee choose
[libjpeg-turbo](https://lists.debian.org/debian-devel-announce/2014/08/msg00000.html)
as the default JPEG decoder.

2010-08-01
[DebConf10](https://debconf10.debconf.org/) starts à New York City, USA

2007-08-05
[Debian Maintainers](https://www.debian.org/vote/2007/vote_003) approved by vote

2009-08-05 Jeff Chimene files bug
[#540000](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=540000) against
live-initramfs.


Calls for help
--------------

#### The Publicity team calls for volunteers and help!

Your Publicity team is asking for help from you our readers, developers, and
interested parties to contribute to the Debian news effort. We implore you to
submit items that may be of interest to our community and also ask for your
assistance with translations of the news into (your!) other languages along
with the needed second or third set of eyes to assist in editing our work
before publishing. If you can share a small amount of your time to aid our
team which strives to keep all of us informed, we need you. Please reach out
to us via IRC on [#debian-publicity](irc://irc.debian.org/debian-publicity)
on [OFTC.net](https://oftc.net/), or our [public mailing list](mailto:debian-publicity@lists.debian.org),
or via email at [press@debian.org](mailto:press@debian.org) for sensitive or
private inquiries.
