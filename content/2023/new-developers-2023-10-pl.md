Title: Nowi Deweloperzy i Opiekunowie Debiana (wrzesień i październik 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published


Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu ostatnich dwóch miesięcy:

  * François Mazen (mzf)
  * Andrew Ruthven (puck)
  * Christopher Obbard (obbardc)
  * Salvo Tomaselli (ltworf)

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich dwóch miesięcy:

  * Bo YU
  * Athos Coimbra Ribeiro
  * Marc Leeman
  * Filip Strömbäck

Gratulacje!

