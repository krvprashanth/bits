Title: Nous desenvolupadors i mantenidors de Debian (juliol i agost del 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Marius Gripsgard (mariogrip)
  * Mohammed Bilal (rmb)
  * Lukas Märdian (slyon)
  * Robin Gustafsson (rgson)
  * David da Silva Polverari (polverari)
  * Emmanuel Arias (eamanu)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Aymeric Agon-Rambosson
  * Blair Noctis
  * Lena Voytek
  * Philippe Coval
  * John Scott

Enhorabona a tots!

