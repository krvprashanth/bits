Title: Chamada de candidaturas (bids) para a DebConf24
Date: 2023-10-31 13:30
Tags: debconf, debconf24, debian
Slug: debconf24-call-for-bids
Author: Debian DebConf Committe
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Status: published

Devido à situação atual em Israel, que sediaria a DebConf24, o comitê da
DebConf decidiu refazer a chamada para sediar a DebConf24 em outro local.

O comitê da DebConf gostaria de expressar seu sincero agradecimento à equipe
israelense da DebConf e ao trabalho que eles realizaram ao longo de vários anos.
No entanto, dada a incerteza sobre a situação, lamentamos que muito
provavelmente não seja possível realizar a DebConf em Israel.

Ao solicitarmos inscrições de locais para sediar o evento, pedimos que você
revise e entenda os detalhes e requisitos para uma submissão de proposta para
sediar a [Conferência de Desenvolvedores(as) Debian](https://www.debconf.org).

Por favor, veja o
[modelo de candidatura para DebConf](https://wiki.debian.org/DebConf/General/Handbook/Bids/LocationCheckList)
para obter as diretrizes sobre como apresentar uma proposta (bid) adequada.

Para enviar uma proposta, por favor crie a(s) página(s) apropriada(s) na
[wiki dos bids para DebConf](https://wiki.debian.org/DebConf/24/Bids) e
adicione-as à seção "Bids" na [DebConf 24](https://wiki.debian.org/DebConf/24).

_Não temos muito tempo para tomar uma decisão. Precisamos de propostas (bids)
até o final de novembro para tomarmos uma decisão até o final do ano._

Após finalizar a sua proposta (bid), mande uma mensagem para
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org) para nos
informar que a sua proposta está pronto para revisão.

Também sugerimos visitar nossa sala de bate-papo no IRC [#debconf-team](https://www.oftc.net).

Devido a este prazo curto, entendemos que as propostas não serão tão completas
como normalmente seriam. Faça o melhor que puder no tempo disponível.

As propostas (bids) serão avaliadas de acordo com a
[lista de prioridades](https://wiki.debian.org/DebConf/General/Handbook/Bids/PriorityList).

Você pode entrar em contato com a equipe da DebConf pelo e-mail
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org), ou
através do canal IRC #debconf-team no OFTC ou via nosso
[canal matrix](https://matrix.to/#/#debconf-team:matrix.debian.social).

Obrigado, 

Comitê da DebConf

