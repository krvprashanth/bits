Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Marius Gripsgard (mariogrip)
  * Mohammed Bilal (rmb)
  * Lukas Märdian (slyon)
  * Robin Gustafsson (rgson)
  * David da Silva Polverari (polverari)
  * Emmanuel Arias (eamanu)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Aymeric Agon-Rambosson
  * Blair Noctis
  * Lena Voytek
  * Philippe Coval
  * John Scott

Xin chúc mừng!

