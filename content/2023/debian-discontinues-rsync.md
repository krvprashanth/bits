Title: archive.debian.org rsync address change
Slug: DebianDiscontinuesRsync
Date: 2023-11-23 08:00
Author: Donald Norwood, Adam D. Barratt
Tags: debian, mirrors, infrastructure, technical 
Status: published

The proposed and previously 
[announced](https://lists.debian.org/debian-mirrors-announce/2023/11/msg00000.html) 
changes to the rsync service have become effective with the
`rsync://archive.debian.org` address now being discontinued.

The worldwide Debian mirrors network has served `archive.debian.org` via
both HTTP and rsync. As part of improving the reliability of the service for
users, the Debian mirrors team is separating the access methods to different
host names:

- `http://archive.debian.org/` will remain the entry point for HTTP
clients such as APT

- `rsync://rsync.archive.debian.org/debian-archive/` is now available for
those who wish to mirror all or parts of the archives.

rsync service on archive.debian.org has stopped, and we encourage anyone using
the service to migrate to the new host name as soon as possible.

If you are currently using rsync to the debian-archive from a debian.org
server that forms part of the archive.debian.org rotation, we also encourage
Administrators to move to the new service name. This will allow us to better
manage which back-end servers offer rsync service in future.

Note that due to its nature the content of archive.debian.org does not
change frequently - generally there will be several months, possibly
more than a year, between updates - so checking for updates more than
once a day is unnecessary.

For additional information please reach out to the
[Debian Mirrors Team](debian-mirrors@lists.debian.org) maillist.
