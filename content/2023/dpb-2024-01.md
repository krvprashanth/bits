Title:  Debian Project Bits Volume 2, Issue 1
Slug: debian-project-bits-2
Date: 2024-01-15 12:00
Authors: Contributor 1, Contributor 2, Contributor 3
Tags: news, dpb, infrastructure
Translation: false
Status: draft

<!-- This is the comment section 
<b> tags must be placed INSIDE of <p> tags

here -->


- - -
**Debian Project Bits**
*Volume 2, Issue 1*
*January 15, 2024*


### Welcome to the second issue of Debian Project Bits!

Those remembering the Debian Weekly News (DwN) will recognize some of the sections here which served as our inspiration.

Debian Project Bits posts will allow for a faster turnaround of some project
news on a monthly basis. The [Debian Micronews](https://micronews.debian.org/)
service will continue to share shorter news items, the [Debian Project News](https://www.debian.org/News/weekly/)
remains as our official newsletter which may move to a biannual archive format.


News
----

#### Interview with Debian Devloper: $InterviewPerson

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis


nostrud exercitation ullamco laboris Lorem ipsum dolor sit amet, consectetur
adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 

#### News from the $X Team

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis


nostrud exercitation ullamco laboris Lorem ipsum dolor sit amet, consectetur
adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 



Events: Upcoming and Reports
----------------------------

### Upcoming 

#### Upcoming item 

Event 1

#### Upcoming item

Event 2

#### Upcoming item

### Reports

#### Report from event
Report 1

#### Report from event
Report 2

#### Report from event
Report 3


Releases
--------

<!-- Any release info from Stable, Oldstable, Sid, or Testing --> 

We made stable even more stable and are completely finished with it. 

Inside Debian
-------------

#### New Debian Members


Please welcome the following newest Debian Project Members:


- Debian Developer 1 \(dd-ldap-entry)
- Debian Developer 2 \(dd-ldap-entry)
- Debian Developer 3 \(dd-ldap-entry)
- Debian Developer 4 \(dd-ldap-entry)


To find out more about our newest members or any Debian Developer, look
for them on the [Debian People list](https://nm.debian.org/public/people/).

#### Other Internal News or Information

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
nostrud exercitation ullamco laboris n!


Security
--------

<!--Not sure we need this if we can figure the script out
the script may pull this automatically.-->

Be sure to update on a fairly regular basis.

Other
-----


#### Popular packages
<!-- Pick 3 of 4 items from popular packages-->


#### New and noteworthy packages in unstable

<!-- Pick 3 of 4 items from unstable-->



#### Once upon a time in Debian:
<!-- Pull 3 or 4 items from the timeline-->

Calls for help
--------------

#### The Publicity team calls for volunteers and help!

Your Publicity team is asking for help from you our readers, developers, and
interested parties to contribute to the Debian news effort. We implore you to
submit items that may be of interest to our community and also ask for your
assistance with translations of the news into (your!) other languages along
with the needed second or third set of eyes to assist in editing our work
before publishing. If you can share a small amount of your time to aid our
team which strives to keep all of us informed, we need you. Please reach out
to us via IRC on [#debian-publicity](irc://irc.debian.org/debian-publicity)
on [OFTC.net](https://oftc.net/), or our [public mailing list](mailto:debian-publicity@lists.debian.org),
or via email at [press@debian.org](mailto:press@debian.org) for sensitive or
private inquiries.
