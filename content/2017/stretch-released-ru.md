Title: Debian 9.0 Stretch выпущен!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: ru
Translator: Lev Lamberov

![Alt Stretch выпущен](|static|/images/banner_stretch.png)

Позвольте фиолетовой резиновой осьминожке вас обнять! Мы рады сообщить о
выпуске Debian 9.0, кодовое имя *Stretch*.

**Хотите его установить?**
Выберите подходящий вам [установочный носитель](https://www.debian.org/distrib/): диски Blu-ray, DVD,
компакт-диски или USB-носители. Затем прочтите [руководство по установке](https://www.debian.org/releases/stretch/installmanual).

**Уже являетесь счастливым пользователем Debian, а теперь хотите просто обновить систему?**
Легко можно выполнить обновление с вашей текущей системы Debian 8,
ознакомьтесь с [информацией о выпуске](https://www.debian.org/releases/stretch/releasenotes).

**Желаете отпраздновать выпуск?**
Разместите [баннер из нашего блога](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) в своём блоге или на веб-сайте!
