Title: Foi lançado Debian 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: pt
Translator: Miguel Figueiredo

![Foi lançado o Stretch](|static|/images/banner_stretch.png)

!Deixa-te abraçar pelo Stretch, o polvo brinquedo de borracha púrpura! Estamos contentes por anunciar a nova versão Debian 9.0, com nome de código *Stretch*.

**Queres instalar o Stretch?**
Escolhe o teu [modo de instalação](https://www.debian.org/distrib/) favorito entre discos Blu-ray, DVDs,
CDs e pens USB. Não te esqueças de ler o [guia de instalação](https://www.debian.org/releases/stretch/installmanual).

**Já és um feliz utilizador de Debian e apenas desejas atualizar?**
Podes atualizar facilmente a partir do teu Debian 8 Jessie, lembra-te apenas de ler antes as [notas de lançamento](https://www.debian.org/releases/stretch/releasenotes).

**Queres celebrar o lançamento de Stretch?**
Partilha [a imagen desta publicação](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) no teu blog ou website!
