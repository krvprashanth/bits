Title: Novos(as) mantenedores(as) Debian (novembro e dezembro de 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as) Debian nos últimos dois meses:

  * Timo Röhling
  * Fabio Augusto De Muzio Tobich
  * Arun Kumar Pariyar
  * Francis Murtagh
  * William Desportes
  * Robin Gustafsson
  * Nicholas Guriev

Parabéns a todos(as)!

