Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Jeroen Ploemen (jcfp)
  * Mark Hindley (leepen)
  * Scarlett Moore (sgmoore)
  * Baptiste Beauplat (lyknode)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Gunnar Ingemar Hjalmarsson
  * Stephan Lachnit

Félicitations !

