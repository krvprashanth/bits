Title: New Debian Developers and Maintainers (March and April 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Jeroen Ploemen (jcfp)
  * Mark Hindley (leepen)
  * Scarlett Moore (sgmoore)
  * Baptiste Beauplat (lyknode)

The following contributors were added as Debian Maintainers in the last two months:

  * Gunnar Ingemar Hjalmarsson
  * Stephan Lachnit

Congratulations!

