Title: 新的 Debian 开发者和维护者 (2021年9月 至 年10月)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

  * Bastian Germann (bage)
  * Gürkan Myczko (tar)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Clay Stan
  * Daniel Milde
  * David da Silva Polverari
  * Sunday Cletus Nkwuda
  * Ma Aiguo
  * Sakirnth Nagarasa

祝贺他们！

