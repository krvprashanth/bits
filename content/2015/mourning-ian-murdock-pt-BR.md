Title: Debian lamenta a perda de Ian Murdock
Date: 2015-12-30 20:15:00
Tags: ian murdock, in memoriam
Slug: mourning-ian-murdock
Lang: pt-BR
Author: Ana Guerrero Lopez, Donald Norwood and Paul Tagliamonte
Translator: Gabriel Francisco
Status: published


![Ian Murdock](|static|/images/ianmurdock.jpg)

Com grande peso no coração Debian lamenta a perda de Ian Murdock, fiel
defensor do Software Livre e de Código Aberto, Pai, Filho e o "ian" de Debian.

Ian iniciou o projeto Debian em Agosto de 1993, lançando as primeiras versões
do Debian logo depois, no mesmo ano. Debian iria se tornar o Sistema
Operacional Universal do mundo, rodando em tudo, desde dispositivos embarcados
até a estação espacial.

O foco nítido de Ian era criar uma Distribuição e cultura de comunidade que
fizessem a coisa certa, tanto eticamente, como tecnicamente. Lançamentos que
são feitos somente quando estão prontos, e a postura firme do projeto sobre a
Liberdade de Software são os padrões de ouro no mundo Livre e de Código Aberto.

A devoção de Ian para realizar as coisas certas guiaram seu trabalho,
tanto no Debian quanto nos anos que vieram, sempre trabalhando pelo
melhor futuro possível.

O sonho de Ian está vivo, a comunidade Debian continua incrivelmente
ativa, com milhares de desenvolvedores trabalhando imensuráveis horas
para trazer ao mundo um sistema operacional confiável e seguro.

Os pensamentos da comunidade Debian estão presentes com a família de Ian
nesse duro momento.

Sua família pede por privacidade nesse momento difícil e é nosso desejo
respeitarmos isso. Condolências de nós, Debian, e da gigantesca comunidade
Linux podem ser enviadas para in-memoriam-ian@debian.org, onde serão mantidas e
arquivadas.
