Title: La DebConf15 dóna la benvinguda a nous patrocinadors
Slug: new-sponsors-debconf15
Date: 2015-03-18 16:00
Author: Laura Arjona Reina
Translator: Adrià García-Alzórriz, Innocent De Marchi, Lluís Gili
Tags: debconf15, debconf, sponsors
Lang: ca
Status: published


L'organització de la **DebConf15** (del 15 al 22 d'agost a Heidelberg,
Alemanya) avança sense problemes, la [crida de propostes és oberta](https://bits.debian.org/2015/03/debconf15-cfp.html)
i avui us volem oferir més novetats sobre els nostres patrocinadors.

Als [nostres nou primers patrocinadors](https://bits.debian.org/2014/11/dc15-welcome-its-first-sponsors.html)
s'hi han afegit dotze noves empreses per recolzar la DebConf15. Moltes
gràcies a tots!

El nostre tercer patrocinador Daurat és la [**Fundació
Matanel**](http://www.matanel.org/), la qual recolza la iniciativa
social arreu del món.

[**IBM**](http://www.ibm.com), l'empresa de tecnologia i consultoria,
s'ha afegit també com a patrocinadora Daurada de la DebConf15.

[**Google**](http://google.com), el cercador i l'empresa de publicitat,
ha augmentat el seu patrocini des de Platejat a Daurat.

[**Mirantis**](http://www.mirantis.com/), [**1&1**](http://www.1und1.de/)
(la qual és també una de les sòcies dels serveis de Debian),
[**MySQL**](http://www.mysql.com/) i
[**Hudson River Trading**](http://www.hudson-trading.com/) han refermat
el seu patrocini Platejat.

I finalment, però no per això menys important, sis patrocinadors més
han acceptat recolzar-nos com a patrocinadors Bronze:
[**Godiug.net**](http://www.godiug.net/),
la [**Universitat de Zuric**](http://www.ifi.uzh.ch/),
[**Deduktiva**](http://www.deduktiva.com/),
[**Docker**](http://www.docker.com/),
[**DG-i**](http://www.dg-i.de/) (el qual és també un soci dels serveis de Debian)
i [**PricewaterhouseCoopers**](http://www.pwc.de/) (la qual ofereix
recolzament de consultoria per a la DebConf15).

L'equip de la DebConf15 està molt agraït a tots els patrocinadors de
la DebConf pel seu recolzament.

## Esdevingueu també un patrocinador!

La DebConf15 encara accepta patrocinadors. Les empreses i
organitzacions interessades poden contactar amb l'equip de la DebConf a
través de [sponsors@debconf.org](mailto:sponsors@debconf.org) o visitar
la pàgina web de la DebConf15 a [http://debconf15.debconf.org](http://debconf15.debconf.org).

