Title: Debian ha sido seleccionado para participar en el Google Summer of Code
Slug: debian-participation-gsoc-2016
Date: 2016-03-13 16:00
Author: Nicolas Dandrimont
Lang: es
Translator: Adrià García-Alzórriz
Tags: announce, gsoc
Status: draft

![GSoC 2016 logo](|static|/images/gsoc2016.jpg)

Por décima vez, Debian ha sido seleccionado como organización tutora 
para el [Google Summer of Code][1] ([página con el programa específico de Debian][2]),
un programa de becas abierto a los estudiantes universitarios mayores 
de 18 años.

[1]: https://summerofcode.withgoogle.com/
[2]: https://wiki.debian.org/SummerOfCode2016

Nuestro equipo de increíbles tutores ha preparado para este año una
[apasionante lista de proyectos][3] y nos gustaría tenerte a bordo para
Debian para alguna de estas becas. El periodo de candidaturas de 
estudiantes se abrirá el 14 de marzo (y se cerrará el 25 de marzo), 
aunque siéntete libre de [registrarte a nuestra lista de correo][4]
y de contactar con nuestros tutores. Puedes también contactarlos  a 
través de nuestro canal de IRC [#debian-soc](irc://irc.debian.org/#debian-soc).

[3]: https://wiki.debian.org/SummerOfCode2016/Projects
[4]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
