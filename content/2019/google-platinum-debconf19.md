Title: Google Platinum Sponsor of DebConf19
Slug: google-platinum-debconf19
Date: 2019-03-25 12:30
Author: Laura Arjona Reina
Artist: Google
Tags: debconf19, debconf, sponsors, Google
Image: /images/google.png
Status: published

[![Googlelogo](|static|/images/google.png)](https://www.google.com)

We are very pleased to announce that [**Google**](https://www.google.com)
has committed to support [DebConf19](https://debconf19.debconf.org) as a **Platinum sponsor**.

*"The annual DebConf is an important part of the Debian development ecosystem
and Google is delighted to return as a sponsor in support of the work
of the global community of volunteers who make Debian and DebConf a reality"*
said Cat Allman, Program Manager in the Open Source Programs
and Making & Science teams at Google.

Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf since more than
ten years, and is also a Debian partner sponsoring parts 
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.


With this additional commitment as Platinum Sponsor for DebConf19,
Google contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Google, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf19 website at [https://debconf19.debconf.org](https://debconf19.debconf.org).
