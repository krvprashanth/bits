Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developersen els darrers dos mesos:

  * Teus Benschop (teusbenschop)
  * Nick Morrott (nickm)
  * Ondřej Kobližek (kobla)
  * Clément Hermann (nodens)
  * Gordon Ball (chronitis)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Nikos Tsipinakis
  * Joan Lledó
  * Baptiste Beauplat
  * Jianfeng Li

Enhorabona a tots!

