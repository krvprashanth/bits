Title: DebConf20 is looking for sponsors!
Slug: debconf20-looking-for-sponsors
Date: 2019-12-10 10:00
Author: Carl J Mannino
Tags: debconf, debconf20, sponsors
Artist: 
Status: draft

<a href="https://debconf20.debconf.org/">DebConf20</a>

<body>
<p>Debian's annual developers conference will be held in<br /> <b>Haifa, Israel</b>
August 23<sup>rd</sup> through 29<sup>th</sup>2020.<br />This annual event attracts Debian
contributors from all around the word to<br />collaborate, share and work in teams on
varies aspects of the Debian operating system.</p>

<p>The DebConf team organizes the annual Debian Conference handling all its
<br />complexity and numerous logistical challenges. The financial contributions<br />and
support by individuals, companies and organizations are essential to<br />our success.</p>

<p>DebConf is always run on a non-profit basis, and organizers work without<br />
financial comprehension. The contribution page can assist you in sponsorship.<br />
 <a href ="https://debconf20.debconf.org/sponsors/become-a-sponsor/">DebConf20</a>
Your generous contribution makes it possible for bringing together<br />Debian
contributors to learn, share, and forge friendships with people from around<br />
the world</p>

