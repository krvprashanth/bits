Title: O Projeto Debian apoia a Fundação GNOME em defesa contra trolls de patentes
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Translator: Rafael Fontenelle
Tags: debian, gnome, patent trolls, fundraising
Lang: pt-BR
Status: published

Em 2012, o Projeto Debian publicou nossa [Posição sobre Patentes de software],
declarando a ameaça que as patentes representam para o Software Livre.

A Fundação GNOME anunciou recentemente que eles estão lutando judicialmente
contra uma alegação de que Shotwell, um gerenciador de fotos pessoal livre
e de código aberto, estaria violando uma patente.

O Projeto Debian apoia firmemente a Fundação GNOME em seus esforços para
mostrar ao mundo que nós nas comunidades de Software Livre nos defenderemos
vigorosamente contra qualquer abuso do sistema de patentes.

Por favor, leia [essa publicação de blog sobre a defesa do GNOME contra este troll de patente]
e considere fazer uma doação para o [Fundo de Defesa do GNOME contra Troll de Patente].

[Posição sobre Patentes de software]: https://www.debian.org/legal/patent
[essa publicação de blog sobre a defesa do GNOME contra este troll de patente]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[Fundo de Defesa do GNOME contra Troll de Patente]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund

