Title: Debian welcomes its new Outreachy intern
Slug: welcome-outreachy-intern-2018-2019
Date: 2018-11-29 20:15
Author: Laura Arjona Reina
Tags: announce, outreachy
Status: published

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

Debian continues participating in Outreachy, and we'd like to 
welcome our new Outreachy intern for this round, lasting from December 2018 to 
March 2019.

[Anastasia Tsikoza](https://moonkin.github.io/)
will work on 
[Improving the integration of Debian derivatives with the Debian infrastructure and the community](https://wiki.debian.org/Outreachy/Round16/Projects/DerivativesIntegration),
mentored by Paul Wise and Raju Devidas.

Congratulations, Anastasia, and welcome!

From [the official website](https://www.outreachy.org/): *Outreachy provides three-month internships
for people from groups traditionally underrepresented in tech.
Interns work remotely with mentors from Free and Open Source Software (FOSS) communities
on projects ranging from programming, user experience, documentation,
illustration and graphical design, to data science.*

The Outreachy programme is possible in Debian thanks
to the efforts of Debian developers and contributors who dedicate
their free time to mentor students and outreach tasks, and
the [Software Freedom Conservancy](https://sfconservancy.org/)'s administrative support,
as well as the continued support of Debian's donors, who provide funding
for the internships.

Join us and help extend Debian! You can follow the work of the Outreachy
interns reading their blogs (they are syndicated in [Planet Debian][planet]),
and chat with us in the #debian-outreach IRC channel and [mailing list](https://lists.debian.org/debian-outreach/).

[planet]: https://planet.debian.org
