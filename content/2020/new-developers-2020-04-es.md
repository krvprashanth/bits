Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Paride Legovini (paride)
  * Ana Custura (acute)
  * Felix Lechner (lechner)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Sven Geuer
  * Håvard Flaget Aasen

¡Felicidades a todos!

