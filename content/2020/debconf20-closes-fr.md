Title: Clôture de DebConf20 en ligne
Date: 2020-08-30 2:40
Tags: debconf20, announce, debconf
Slug: debconf20-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Aigars Mahinovs
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

[![Photo de groupe de DebConf20 - cliquez pour agrandir](|static|/images/debconf20_group_small.jpg)](https://wiki.debian.org/DebConf/20/GroupPhoto)


Samedi 29 août 2020, la conférence annuelle des développeurs et
contributeurs Debian s'est achevée.

DebConf20 s'est tenue en ligne pour la première fois du fait de la
pandémie du coronavirus (COVID-19).

Toutes les sessions ont été diffusées en direct à travers différents canaux
pour participer à la conférence : messagerie IRC, édition de texte
collaboratif en ligne et salons de visioconférence.

Avec plus de 850 participants de 80 pays différents et un total de plus de
100 présentations, sessions de discussion, sessions spécialisées (BoF) et
d'autres activités,
[DebConf20](https://debconf20.debconf.org) a été un énorme succès.

Dès qu'il est devenu évident que DebConf20 allait être un événement en
ligne, l'équipe vidéo de DebConf a passé beaucoup de temps les mois suivants
pour adapter, améliorer et dans certains cas écrire du début à la fin les
technologies nécessaires pour rendre possible la tenue de DebConf en ligne.
À partir des leçons tirées de la MiniDebConf en ligne fin mai, certaines
adaptations ont été réalisées et finalement nous avons mis au point une
configuration intégrant Jitsi, OBS, Voctomix, SReview, nginx, Etherpad et
un frontal récemment écrit basé sur le web pour Voctomix, pour former
l'ensemble des différents éléments nécessaires.

Toutes les composantes de l'infrastructure vidéo sont des logiciels libres
et l'ensemble des réglages est configuré au moyen de leur dépôt public
[ansible](https://salsa.debian.org/debconf-video-team/ansible).

Le [programme](https://debconf20.debconf.org/schedule/) de
DebConf20 comprenait également deux programmes particuliers en une autre
langue que l'anglais : la MiniConf en espagnol avec huit communications en
deux jours et la MiniConf en malayalam avec neuf communications en trois
jours. Il a aussi été possible de réaliser, de diffuser et d'enregistrer
des activités ponctuelles introduites par les participants pendant toute la
durée de la conférence. Plusieurs équipes se sont aussi réunies pour des
[rencontres](https://wiki.debian.org/Sprints/) sur certains domaines de
développement de Debian.

Entre les présentations, le flux vidéo présentait en boucle les parrains
habituels et également d'autres clips comprenant des photos des DebConf
précédentes, des anecdotes amusantes sur Debian et de courtes dédicaces
vidéos envoyées par des participants pour communiquer avec leurs amis de
Debian.

Pour tous ceux qui n'ont pu participer à la DebConf, la plupart des
communications et des sessions sont déjà disponibles sur le
[site web des réunions Debian](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/),
et les dernières le seront dans les prochains jours.

Le site web de [DebConf20](https://debconf20.debconf.org/) restera actif à
fin d'archive et continuera à offrir des liens vers les présentations et
vidéos des communications et des événements.

L'an prochain, il est prévu que [DebConf21](https://wiki.debian.org/DebConf/21)
se tienne à Haïfa en Israël, en août ou septembre.

DebConf s'est engagée à offrir un environnement sûr et accueillant pour
tous les participants. Durant la conférence, plusieurs équipes (le
secrétariat, l'équipe d'accueil et l'équipe communauté) étaient disponibles
pour offrir aux participants le meilleur accueil à la conférence, et
trouver des solutions à tout problème qui aurait pu subvenir. Voir la
[page web à propos du Code de conduite sur le site de DebConf20](https://debconf20.debconf.org/about/coc/)
pour plus de détails à ce sujet.


Debian remercie les nombreux [parrains](https://debconf20.debconf.org/sponsors/)
pour leur engagement dans leur soutien à DebConf20, et en particulier ses
parrains de platine :
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
et
[**Amazon Web Services (AWS)**](https://aws.amazon.com/).


### À propos de Debian

Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et prenant en charge un grand nombre de types d'ordinateurs, la
distribution Debian est conçue pour être le _système d'exploitation universel_.

### À propos de DebConf

DebConf est la conférence des développeurs du projet Debian. En plus
d'un programme complet de présentations techniques, sociales ou
organisationnelles, DebConf fournit aux développeurs, aux contributeurs et
à toute personne intéressée, une occasion de rencontre et de travail
collaboratif interactif. DebConf a eu lieu depuis 2000 en des endroits du
monde aussi variés que l'Écosse, l'Argentine ou la Bosnie-Herzégovine. Plus
d'information sur DebConf est disponible à l'adresse
[https://debconf.org/](https://debconf.org).


### À propos de Lenovo

En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des solutions
pour la domotique ou le bureau connecté et les centres de données,
[**Lenovo**](https://www.lenovo.com) a compris combien étaient essentiels
les plateformes et systèmes ouverts pour un monde connecté..

### À propos d'Infomaniak

[**Infomaniak**](https://www.infomaniak.com) est la plus grande
compagnie suisse d'hébergement web qui offre aussi des services de
sauvegarde et de stockage, des solutions pour les organisateurs d'événement
et des services de diffusion en direct et de vidéo à la demande. Elle
possède l'intégralité de ses centres de données et de tous les éléments
essentiels pour le fonctionnement des services et produits qu'elle fournit
(à la fois sur le plan matériel et logiciel).

### À propos de Google

[**Google**](https://google.com/) est l'une des plus grandes
entreprises technologiques du monde qui fournit une large gamme de services
relatifs à Internet et de produits tels que des technologies de publicité
en ligne, des outils de recherche, de l'informatique dans les nuages, des
logiciels et du matériel.

Google apporte son soutien à Debian en parrainant la DebConf depuis plus de
dix ans, et est également un partenaire de Debian en parrainant les
composants de l'infrastructure d'intégration continue de
[Salsa](https://salsa.debian.org) au sein de la plateforme Google Cloud.

### À propos d'Amazon Web Services (AWS)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) est une des
plateformes de nuage les plus complètes et largement adoptées au monde,
proposant plus de 175 services complets issus de centres de données du
monde entier (dans 77 zones de disponibilité dans 24 régions géographiques).
Parmi les clients d'AWS sont présentes des jeunes pousses à croissance
rapide, de grandes entreprises et des organisations majeures du secteur
public.

### Plus d'informations

Pour plus d'informations, veuillez consulter la page internet de
DebConf20 à l'adresse
[https://debconf20.debconf.org/](https://debconf20.debconf.org/)
ou envoyez un message à <press@debian.org>.
