Title: Debians officiella kommunikationskanaler
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announce
Lang: sv
Translator: Andreas Rönnquist
Status: published

Då och då får vi frågor i Debian om våra officiella kommunikationskanaler
och frågor om statusen för Debian om vilka som kan äga liknande namngivna
webbplatser.

Debians huvudwebbplats [www.debian.org](https://www.debian.org)
är vårt primära kommunikationsmedium. Dom som söker information om
aktuella evenemang och utvecklingsframsteg i gemenskapen kan vara intresserade
av [Debiannyheter](https://www.debian.org/News/)-sektionen av
Debians webbplats.

För mindre formella tillkännagivanden har vi den officiella bloggen
[Bits from Debian](https://bits.debian.org), samt
tjänsten [Debian micronews](https://micronews.debian.org) för
kortare nyheter.

Vårt officiella nyhetsbrev
[Debians projektnyheter](https://www.debian.org/News/weekly/)
och alla våra officiella tillkännagivanden av nyheter eller projektförändringar
postas både på vår webbsida och vår officiella sändlista
[debian-announce](https://lists.debian.org/debian-announce/) eller
[debian-news](https://lists.debian.org/debian-news/).
Publicering till dessa sändlistor är begränsat.

Vi vill även ta denna möjlighet att tillkännage hur Debianprojektet,
eller förkortat Debian, är strukturerat.

Debian har en struktur som reguleras av våra
[stadgar](https://www.debian.org/devel/constitution).
Tjänstemän och delegerade medlemmar listas på vår sida för vår
[organisationsstruktur](https://www.debian.org/intro/organization).
Ytterligare grupper listas på vår sida för [grupper](https://wiki.debian.org/Teams).

Den fullständiga listan på officiella Debianmedlemmar kan hittas på vår
sida
[Nya medlemmar](https://nm.debian.org/members),
där våra medlemskap hanteras. En mer fullständig lista på bidragslämnare till
Debian kan hittas på vår sida
[Bidragslämnare](https://contributors.debian.org).

Om du har frågor kan du (på engelska) nå pressgruppen på
[press@debian.org](mailto:press@debian.org).
